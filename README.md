## Selection of database

We selected the postgreSQL database for our storage and usage mainly because it's not tied to any specific platform and it does play nice with all different development needs

---
## Table Relationship Diagram 
Database [Image of Model](https://github.com/taylja/images/blob/main/project20210412.png)

---
## Schema's
We have one schema within our postreSQL database called software_factory which has a table 'project' that is there to hold each schema name needed for our development needs

Each project should have it's own schema thereby separating and eliminating any collision possibilities.

TABLE software_factory.goal

    id bigint NOT NULL DEFAULT nextval('software_factory.goal_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    description character varying COLLATE pg_catalog."default",
    comments text COLLATE pg_catalog."default",
    CONSTRAINT goal_pkey PRIMARY KEY (id),
    CONSTRAINT uq_goal_id UNIQUE (id)

TABLE software_factory.project

    id bigint NOT NULL DEFAULT nextval('software_factory.project_id_seq'::regclass),
    schema_name character varying COLLATE pg_catalog."default" NOT NULL,
    budget numeric(10,2),
    manager character varying COLLATE pg_catalog."default",
    primary_developer character varying COLLATE pg_catalog."default",
    pri_goal_id bigint,
    start_date timestamp without time zone NOT NULL,
    completed_date timestamp without time zone,
    end_date timestamp without time zone,
    comments text COLLATE pg_catalog."default",
    CONSTRAINT project_pkey PRIMARY KEY (id),
    CONSTRAINT uq_project_id UNIQUE (id)
        INCLUDE(id),
    CONSTRAINT fk_project_pri_goal FOREIGN KEY (pri_goal_id)
        REFERENCES software_factory.goal (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

TABLE software_factory.projectgoal

    id bigint NOT NULL DEFAULT nextval('software_factory.projectgoal_id_seq'::regclass),
    date_assigned timestamp without time zone DEFAULT now(),
    priority bigint,
    goal_id bigint,
    comments text COLLATE pg_catalog."default",
    CONSTRAINT project_goal_pkey PRIMARY KEY (id),
    CONSTRAINT uq_projectgoal_id UNIQUE (id)
        INCLUDE(id),
    CONSTRAINT fk_projectgoal_goal FOREIGN KEY (goal_id)
        REFERENCES software_factory.goal (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

TABLE software_factory.xref_project_project_goal

    project_id bigint NOT NULL,
    projectgoal_id bigint NOT NULL,
    date_added time without time zone DEFAULT now(),
    added_by character varying COLLATE pg_catalog."default",
    comments text COLLATE pg_catalog."default",
    CONSTRAINT xref_project_project_goal_pkey PRIMARY KEY (project_id, projectgoal_id),
    CONSTRAINT uq_project_project_goal UNIQUE (project_id, projectgoal_id),
    CONSTRAINT fk_xref_project_projectgoal FOREIGN KEY (projectgoal_id)
        REFERENCES software_factory.projectgoal (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_xref_project_id FOREIGN KEY (project_id)
        REFERENCES software_factory.project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
